
#ifdef RCT_NEW_ARCH_ENABLED
#import "RNLRGEYODRNSpec.h"

@interface LRGEYODRN : NSObject <NativeLRGEYODRNSpec>
#else
#import <React/RCTBridgeModule.h>

@interface LRGEYODRN : NSObject <RCTBridgeModule>
#endif

@end
