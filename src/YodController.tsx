import {NativeModules, Platform} from 'react-native';
const {YodChannel} = NativeModules;
import axios from 'axios';

const LINKING_ERROR =
  `The package 'lrge_yod_rn' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

class YodController {
	public cpf: any;
	public boardingCode: any;
	public apiToken: any;

    constructor(apiToken:String, cpf:String, boardingCode:String) {
        this.cpf = cpf;
        this.boardingCode = boardingCode;
        this.apiToken = apiToken;
        if(YodChannel == null){
          new Proxy(
            {},
            {
              get() {
                throw new Error(LINKING_ERROR);
              },
            }
          )
        }
    }

    start(){
        YodChannel.deleteReport('/path/noneed/');
        YodChannel.setControlParams(
          2.77778, 
          4.0,
          0.1
        );
        YodChannel.start('/path/noneed/');
    }

     
    stop(){
        var self = this;
        YodChannel.stop('/path/noneed/');
        setTimeout(function(){
            function sendToServer(reportList: any[], tryC:number){
                let data = {
                    "boardingData": reportList.map((e)=>{
                        return {
                            "lat": parseFloat(e[9]),
                            "lng": parseFloat(e[10]),
                            "altitude": parseFloat(e[11]),
                            "speed": parseFloat(e[16]),
                            "speedAccuracy": parseFloat(e[17]),
                            "accuracy": parseFloat(e[12]),
                            "ts": (e[11])
                          };
                    }),
                    "boardingCode": self.boardingCode
                };
                console.log("Sending data to server", data);
                return axios.post('https://yod.lrgesystems.com/api/v1/boardings/'+self.cpf+'/register?api_token='+self.apiToken, data)
                  .then(function (response) {
                    console.log("Report sent");
                    console.log("Response:");
                    console.log(response.data);
                  })
                  .catch(function (error) {
                    console.error("YOD ERROR SENDING TO SERVER");
                    console.error(error.response.data);
                    console.warn("TRYING AGAIN");
                    if(tryC > 3){
                      console.error("YOD ERROR SENDING TO SERVER");
                      console.error("GIVING UP");
                    }else{
                      sendToServer(reportList, tryC+1).catch((_)=>{
                      });
                    }
                  });
            };
            YodChannel.getReportData('/path/noneed/').then((reportList: any[])=>{
                sendToServer(reportList, 1);
            });
        }, 500);
    }

    async getLastPayload(){
        return await YodChannel.getLastPayload();
    }

    async getScore(){
        var self = this;
        return axios.get('https://yod.lrgesystems.com/api/v1/boardings/'+self.boardingCode+'/score?api_token='+self.apiToken)
                  .then(function (response) {
                    console.log('get score ');
                    console.log('Response:');
                    console.log(response.data);
                    return response.data;
                  })
                  .catch(function (error) {
                    console.error('YOD ERROR GETTING SCORE');
                    console.error('BoardingCode:', self.boardingCode);
                    console.error(error.response.data);
                    return error;
                  });
    }
}

export default YodController;