package com.lrgeyodrn.yod;

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.*
import android.provider.Settings
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.room.Room
import com.lrgeyodrn.yod.db.AppDatabase
import com.lrgeyodrn.yod.db.LocationPayload
import com.lrgeyodrn.yod.db.Migrations
import com.google.android.gms.location.*
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.math.absoluteValue
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin


class EndlessService : Service() {

    companion object {
        val LOCK = ReentrantLock();
    }

    private var wakeLock: PowerManager.WakeLock? = null
    private var isServiceStarted = false
    private var TARGET_PATH:String? = null
    private var CURRENT_ACTION:String = Actions.STOP.name

    private var MIN_BEARING = 0.0;
    private var MIN_ACCELERATION = 0.0;
    private var MIN_SPEED = 0.0;

    override fun onBind(intent: Intent): IBinder? {
        log("Some component want to bind with the service")
        // We don't provide binding, so return null
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        log("onStartCommand executed with startId: $startId")
        if (intent != null) {
            val action = intent.action
            CURRENT_ACTION = action!!
            TARGET_PATH = intent.getStringExtra("path");

            log("using an intent with action $action")
            when (action) {
                Actions.START.name -> startService()
                Actions.STOP.name -> stopService()
                else -> log("This should never happen. No action in the received intent")
            }
        } else {
            log(
                    "with a null intent. It has been probably restarted by the system."
            )
        }
        // by returning this we make sure the service is restarted if the system kills the service
        return START_STICKY
    }
    var DB:AppDatabase? = null

    override fun onCreate() {
        super.onCreate()
        log("The service has been created".toUpperCase())
        DB = Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java, "yod"
        ).allowMainThreadQueries()
                .addMigrations(
                        Migrations.MIGRATION_1_2,
                        Migrations.MIGRATION_2_3,
                        Migrations.MIGRATION_3_4,
                )
                .fallbackToDestructiveMigration()
                .build()
        log("DB")
        log(DB.toString())
        val a = DB!!.controlParamsDAO().getParams()
        MIN_ACCELERATION = a.min_acceleration
        MIN_BEARING = a.min_bearing
        MIN_SPEED = a.min_speed
        val notification = createNotification()
        startForeground(1, notification)
    }

    override fun onDestroy() {
        super.onDestroy()
        log("The service has been destroyed".toUpperCase())
        Toast.makeText(this, "Análise finalizada", Toast.LENGTH_SHORT).show()
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        val restartServiceIntent = Intent(applicationContext, EndlessService::class.java).also {
            it.setPackage(packageName)
            it.putExtra("path", TARGET_PATH);
            it.action = CURRENT_ACTION
        };
        val pm = applicationContext.packageManager;
        val intent = pm.getLaunchIntentForPackage(applicationContext.packageName);
        val pendingIntent: PendingIntent = intent.let { notificationIntent ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent.getService(this, 1, restartServiceIntent, PendingIntent.FLAG_MUTABLE);
            } else {
                PendingIntent.getService(this, 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
            }
        }
        val restartServicePendingIntent: PendingIntent = pendingIntent
        applicationContext.getSystemService(Context.ALARM_SERVICE);
        val alarmService: AlarmManager = applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager;
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePendingIntent);
    }

    private fun startService() {
        if (isServiceStarted) return
        log("Starting the foreground service task")
        Toast.makeText(this, "Análise iniciando", Toast.LENGTH_SHORT).show()
        isServiceStarted = true
        setServiceState(this, ServiceState.STARTED)

        // we need this lock so our service gets not affected by Doze Mode
        wakeLock =
                (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
                    newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "EndlessService::lock").apply {
                        acquire()
                    }
                }

        // we're starting a loop in a coroutine
        GlobalScope.launch(Dispatchers.IO) {
//            while (isServiceStarted) {
//                launch(Dispatchers.IO) {
                    startLocationUpdates()
//                }
//                delay(5 * 1000)
//            }
//            log("End of the loop for the service")
        }

    }

    private fun stopService() {
        log("Stopping the foreground service")
        Toast.makeText(this, "Análise finalizando", Toast.LENGTH_SHORT).show()
        try {
            wakeLock?.let {
                if (it.isHeld) {
                    it.release()
                }
            }
            stopForeground(true)
            stopSelf()
        } catch (e: Exception) {
            log("Service stopped without being started: ${e.message}")
        }
        isServiceStarted = false
        setServiceState(this, ServiceState.STOPPED)
        if(mFusedLocationClient != null && mLocationCallback != null){
            mFusedLocationClient!!.removeLocationUpdates(mLocationCallback!!)
        }
    }

    fun updateResultLocation(locationResult: LocationResult){
        log("UPDATING LOCATION RESULT")
        locationResult.lastLocation.let {
            if(it != null){
                log("RESULT OK")
                log(it.toString());
                val payload = HashMap<String, Any>();
                payload["accuracy"] = it.accuracy;
                payload["altitude"] = it.altitude;
                payload["bearingAccuracyDegrees"] = 0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    payload["elapsedRealtimeAgeMillis"] = it.elapsedRealtimeAgeMillis
                };
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    payload["elapsedRealtimeMillis"] = it.elapsedRealtimeMillis
                }
                payload["elapsedRealtimeNanos"] = it.elapsedRealtimeNanos
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    payload["elapsedRealtimeUncertaintyNanos"] = it.elapsedRealtimeUncertaintyNanos
                }
                payload["latitude"] = it.latitude
                payload["longitude"] = it.longitude
                payload["speed"] = it.speed
                payload["time"] = it.time
                payload["verticalAccuracyMeters"] = it.verticalAccuracyMeters
                payload["speedAccuracyMetersPerSecond"] = it.speedAccuracyMetersPerSecond
                if(it.provider != null){
                    payload["provider"] = it.provider!!
                }
                if(it.extras?.getInt("satellites") != null){
                    payload["satellites"] = (it.extras?.getInt("satellites"))!!
                }

                log("BEARING: ")
                log(payload["bearing"].toString())


                DB!!.locationPayloadDAO().insertAll(LocationPayload(
                        null,
                        "0",
                        "0",
                        "0",
                        "0",
                        "0",
                        "0",
                        "0",
                        "0",
                        "0",
                        payload["latitude"].toString(),
                        payload["longitude"].toString(),//       SegAnalytics.gpsData?.longitude.toString() ?? '',
                        payload["time"].toString(),//       nowTs.toString(),
                        payload["accuracy"].toString(),//       SegAnalytics.gpsData?.accuracy.toString() ?? '',
                        payload["altitude"].toString(),//       SegAnalytics.gpsData?.altitude.toString() ?? '',
                        "0",//       SegAnalytics.gpsData?.heading.toString() ?? '',
                        payload["bearingAccuracyDegrees"].toString(),//       SegAnalytics.gpsData?.headingAccuracy.toString() ?? '',
                        payload["satellites"].toString(),//       SegAnalytics.gpsData?.satelliteNumber.toString() ?? '',
                        payload["speed"].toString(),//       SegAnalytics.gpsData?.speed.toString() ?? '',
                        payload["speedAccuracyMetersPerSecond"].toString(),//
                        TARGET_PATH!!,
                        getDeviceName(),
                        0.0,
                        0.0
                ))

            }
        }

    }

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationCallback: LocationCallback? = null
    private var mLocationRequest: LocationRequest? = null

    private fun startLocationUpdates() {
        if(mFusedLocationClient == null){
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(applicationContext);
//            mFusedLocationClient.
            mLocationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)

                    updateResultLocation(locationResult)
                }
            }
            mLocationRequest = LocationRequest.create().apply {
                interval = 1000
                fastestInterval = 1000
                priority =  Priority.PRIORITY_HIGH_ACCURACY
                maxWaitTime = 1000
            }
            if (ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }else{
                mFusedLocationClient!!.requestLocationUpdates(mLocationRequest!!,
                        mLocationCallback as LocationCallback,
                        Looper.getMainLooper())

            }

        }

    }

    private fun createNotification(): Notification {
        val notificationChannelId = "ENDLESS SERVICE CHANNEL"

        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(
                    notificationChannelId,
                    "Endless Service notifications channel",
                    NotificationManager.IMPORTANCE_HIGH
            ).let {
                it.description = "Endless Service channel"
                it.enableLights(true)
                it.lightColor = Color.RED
                it.enableVibration(true)
                it.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                it
            }
            notificationManager.createNotificationChannel(channel)
        }

        val pm = applicationContext.packageManager;
        val intent = pm.getLaunchIntentForPackage(applicationContext.packageName);

        val pendingIntent: PendingIntent = intent.let { notificationIntent ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_MUTABLE)
            } else {
                PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT)
            }
        }

        val builder: Notification.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) Notification.Builder(
                this,
                notificationChannelId
        ) else Notification.Builder(this)

        return builder
                .setContentTitle("YOD-7")
                .setContentText("")
                .setContentIntent(pendingIntent)
//                .setSmallIcon(com.anonymous.DemoSmartload.R.mipmap.ic_launcher)
                .setTicker("")
                .setPriority(Notification.PRIORITY_HIGH) // for under android 26 compatibility
                .build()
    }
}
