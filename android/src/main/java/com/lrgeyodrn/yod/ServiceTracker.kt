package com.lrgeyodrn.yod;

import android.content.Context
import android.content.SharedPreferences
import org.json.JSONArray

enum class ServiceState {
    STARTED,
    STOPPED,
}

private const val name = "SPYSERVICE_KEY"
private const val key = "SPYSERVICE_STATE"

fun setServiceState(context: Context, state: ServiceState) {
    val sharedPrefs = getPreferences(context)
    sharedPrefs.edit().let {
        it.putString(key, state.name)
        it.apply()
    }
}

fun getServiceState(context: Context): ServiceState {
    val sharedPrefs = getPreferences(context)
    val value = sharedPrefs.getString(key, ServiceState.STOPPED.name)
    return ServiceState.valueOf(value!!)
}

//fun setLastPayload(context: Context, payload: List<String>) {
//    val sharedPrefs = getPreferences(context)
//    sharedPrefs.edit().let {
//        it.putString("lastPayload", JSONArray(payload).toString())
//        it.apply()
//    }
//}
//
//fun getLastPayload(context: Context): List<String> {
//    val sharedPrefs = getPreferences(context)
//    val value = JSONArray(sharedPrefs.getString("lastPayload", "[" +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"," +
//            "\"\"]"))
//    return listOf<String>(
//            value[0] as String,
//            value[1] as String,
//            value[2] as String,
//            value[3] as String,
//            value[4] as String,
//            value[5] as String,
//            value[6] as String,
//            value[7] as String,
//            value[8] as String,
//            value[9] as String,
//            value[10] as String,
//            value[11] as String,
//            value[12] as String,
//            value[13] as String,
//            value[14] as String,
//            value[15] as String,
//            value[16] as String,
//            value[17] as String,
//            value[18] as String,
//    )
//}

private fun getPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences(name, 0)
}
