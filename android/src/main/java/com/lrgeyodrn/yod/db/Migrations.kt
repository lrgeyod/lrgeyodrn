package com.lrgeyodrn.yod.db;

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase


class Migrations {
    object MIGRATION_1_2: Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE location_payload "
                    + "ADD COLUMN modelId TEXT NOT NULL DEFAULT \"\" ")
        }
    }

    object MIGRATION_2_3: Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE location_payload "
                    + "ADD COLUMN deltaBearing DOUBLE ")
            database.execSQL("ALTER TABLE location_payload "
                    + "ADD COLUMN accelerationCurve DOUBLE ")
        }
    }

    object MIGRATION_3_4: Migration(3, 4) {
        override fun migrate(database: SupportSQLiteDatabase) {
        }
    }
}
