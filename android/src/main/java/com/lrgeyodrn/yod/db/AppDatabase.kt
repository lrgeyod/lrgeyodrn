package com.lrgeyodrn.yod.db;

import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
        entities = [LocationPayload::class, ControlParams::class],
        version = 5,
        exportSchema = false
//        exportSchema = true,
//        autoMigrations = [
//            AutoMigration (from=3, to=4),
//            AutoMigration (from=2, to=4),
//            AutoMigration (from=2, to=3),
//            AutoMigration (from=4, to=5),
//        ],

)
abstract class AppDatabase : RoomDatabase() {

    abstract fun locationPayloadDAO(): LocationPayloadDAO
    abstract fun controlParamsDAO(): ControlParamsDAO
}
