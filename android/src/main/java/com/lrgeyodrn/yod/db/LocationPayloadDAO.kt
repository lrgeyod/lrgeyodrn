package com.lrgeyodrn.yod.db;

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface LocationPayloadDAO {
    @Query("SELECT * FROM location_payload")
    fun getAll(): List<LocationPayload>

    @Query("SELECT * FROM location_payload ORDER BY uid DESC LIMIT 1")
    fun getLast(): LocationPayload?

    @Query("SELECT * FROM location_payload ORDER BY uid DESC LIMIT :limit")
    fun getLasts(limit:Int): List<LocationPayload>

    @Insert
    fun insertAll(vararg location_payloads: LocationPayload?)

    @Delete
    fun delete(location_payload: LocationPayload)

    @Query("DELETE FROM location_payload")
    fun deleteAll()

    @Query("SELECT report_name FROM location_payload GROUP BY report_name ORDER BY uid DESC")
    fun reports():List<String>

    @Query("SELECT * FROM location_payload WHERE report_name = :name")
    fun reportData(name:String): List<LocationPayload>

    @Query("DELETE FROM location_payload WHERE report_name = :reportName")
    fun deleteByReport(reportName:String)
}
