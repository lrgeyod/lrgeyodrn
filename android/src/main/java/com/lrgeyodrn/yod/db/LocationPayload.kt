package com.lrgeyodrn.yod.db;

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "location_payload")
data class LocationPayload(
        @PrimaryKey val uid: Int?,
        @ColumnInfo(name = "accelerometerEvent_x") val accelerometerEvent_x: String="0",
        @ColumnInfo(name = "accelerometerEvent_y") val accelerometerEvent_y: String="0",
        @ColumnInfo(name = "accelerometerEvent_z") val accelerometerEvent_z: String="0",
        @ColumnInfo(name = "gyroscopeEvent_x") val gyroscopeEvent_x: String="0",
        @ColumnInfo(name = "gyroscopeEvent_y") val gyroscopeEvent_y: String="0",
        @ColumnInfo(name = "gyroscopeEvent_z") val gyroscopeEvent_z: String="0",
        @ColumnInfo(name = "magnetometerEvent_x") val magnetometerEvent_x: String="0",
        @ColumnInfo(name = "magnetometerEvent_y") val magnetometerEvent_y: String="0",
        @ColumnInfo(name = "magnetometerEvent_z") val magnetometerEvent_z: String="0",
        @ColumnInfo(name = "latitude") val latitude: String="0",
        @ColumnInfo(name = "longitude") val longitude: String="0",
        @ColumnInfo(name = "time") val time: String="0",
        @ColumnInfo(name = "accuracy") val accuracy: String="0",
        @ColumnInfo(name = "altitude") val altitude: String="0",
        @ColumnInfo(name = "bearing") val bearing: String="0",
        @ColumnInfo(name = "bearingAccuracyDegrees") val bearingAccuracyDegrees: String="0",
        @ColumnInfo(name = "satellites") val satellites: String="0",
        @ColumnInfo(name = "speed") val speed: String="0",
        @ColumnInfo(name = "speedAccuracyMetersPerSecond") val speedAccuracyMetersPerSecond: String="0",
        @ColumnInfo(name = "report_name") val reportName: String="0",
        @ColumnInfo(name = "modelId") val modelId: String="",
        @ColumnInfo(name = "deltaBearing") val deltaBearing: Double?=null,
        @ColumnInfo(name = "accelerationCurve") val accelerationCurve: Double?=null
        ) {
}
