package com.lrgeyodrn.yod.db;

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ControlParamsDAO {

    @Query("SELECT * FROM control_params ORDER BY uid DESC LIMIT 1")
    fun getParams(): ControlParams

    @Insert
    fun addParams(vararg control_params: ControlParams?)

    fun setParams(vararg control_params: ControlParams?){
        deleteAll()
        addParams(*control_params)
    }

    @Query("DELETE FROM control_params")
    fun deleteAll()

}
