package com.lrgeyodrn.yod.db;

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "control_params")
data class ControlParams(
        @PrimaryKey val uid: Int?,
        @ColumnInfo(name = "min_speed") val min_speed: Double,
        @ColumnInfo(name = "min_acceleration") val min_acceleration: Double,
        @ColumnInfo(name = "min_bearing") val min_bearing: Double,
        )
