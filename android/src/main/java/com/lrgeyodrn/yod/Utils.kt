package com.lrgeyodrn.yod;

import android.os.Build
import android.util.Log
import java.lang.Math.*
import java.lang.Math.sqrt
import java.util.*
import kotlin.math.*
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin

fun log(msg: String) {
    Log.d("ENDLESS-SERVICE", msg)
}

fun getDeviceName(): String {
    val manufacturer: String = Build.MANUFACTURER
    val model: String = Build.MODEL

    return if (model.lowercase(Locale.getDefault()).startsWith(manufacturer.lowercase(Locale.getDefault()))) {
        capitalize(model) + " "+ Build.DEVICE
    } else {
        capitalize(manufacturer) + " " + model + " " + Build.DEVICE
    }
}


private fun capitalize(s: String?): String {
    if (s == null || s.length == 0) {
        return ""
    }
    val first = s[0]
    return if (Character.isUpperCase(first)) {
        s
    } else {
        first.uppercaseChar().toString() + s.substring(1)
    }
}
