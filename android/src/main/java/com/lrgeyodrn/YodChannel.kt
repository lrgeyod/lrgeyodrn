package com.lrgeyodrn

import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.room.Room
import com.lrgeyodrn.yod.*
import com.lrgeyodrn.yod.db.AppDatabase
import com.lrgeyodrn.yod.db.ControlParams
import com.lrgeyodrn.yod.db.Migrations
import com.facebook.react.bridge.*
import kotlin.jvm.internal.Intrinsics

class YodChannel(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
    private lateinit var db:AppDatabase;
    override fun getName(): String {
        return "YodChannel"
    }

    @ReactMethod
    fun start(location: String) {

        Log.d("YodChannel", "location: $location")

        this.setServiceStateIntern(true, location);
    }

    @ReactMethod
    fun stop(location: String) {

        Log.d("YodChannel", "location: $location")

        this.setServiceStateIntern(false, location);
    }

    private fun actionOnService(action: Actions, path:String) {
        if (getServiceState(reactApplicationContext) == ServiceState.STOPPED && action == Actions.STOP) return
        Intent(reactApplicationContext, EndlessService::class.java).also {
            it.action = action.name
            it.putExtra("path", path);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                log("Starting the service in >=26 Mode")
                reactApplicationContext.startForegroundService(it)
                return
            }
            log("Starting the service in < 26 Mode")
            reactApplicationContext.startService(it)
        }
    }

    private fun setServiceStateIntern(state: Boolean, path:String){
        val arg = if(state) Actions.START else Actions.STOP;
        actionOnService(arg, path)
    }

    @ReactMethod
    fun setServiceState(state: Boolean, path:String, promise: Promise){
        this.setServiceStateIntern(state, path);
        promise.resolve(null)
    }

    @ReactMethod
    fun getLastPayload(promise: Promise){
        val payload = db.locationPayloadDAO().getLast();
        if(payload != null){
            val a = WritableNativeArray();

            a.pushString(payload.accelerometerEvent_x);
            a.pushString(payload.accelerometerEvent_y);
            a.pushString(payload.accelerometerEvent_z);
            a.pushString(payload.gyroscopeEvent_x);
            a.pushString(payload.gyroscopeEvent_y);
            a.pushString(payload.gyroscopeEvent_z);
            a.pushString(payload.magnetometerEvent_x);
            a.pushString(payload.magnetometerEvent_y);
            a.pushString(payload.magnetometerEvent_z);
            a.pushString(payload.latitude);
            a.pushString(payload.longitude);
            a.pushString(payload.time);
            a.pushString(payload.accuracy);
            a.pushString(payload.altitude);
            a.pushString(payload.bearing);
            a.pushString(payload.bearingAccuracyDegrees);
            a.pushString(payload.speed);
            a.pushString(payload.speedAccuracyMetersPerSecond);
            a.pushString(payload.modelId);
            a.pushString(payload.deltaBearing.toString());
            a.pushString(payload.deltaBearing.toString());
            a.pushString(payload.accelerationCurve.toString());
            promise.resolve(a)
        }else{
            promise.reject("NO_DATA: No pauload yet on device")
        }
    }

    @ReactMethod
    fun getReportsName(promise: Promise){
        promise.resolve(db.locationPayloadDAO().reports())
    }

    @ReactMethod
    fun getReportData(name:String, promise: Promise){
        val fn = WritableNativeArray();
        (db.locationPayloadDAO().reportData(name).forEach {
            val a = WritableNativeArray();
            a.pushString(it.accelerometerEvent_x);
            a.pushString(it.accelerometerEvent_y);
            a.pushString(it.accelerometerEvent_z);
            a.pushString(it.gyroscopeEvent_x);
            a.pushString(it.gyroscopeEvent_y);
            a.pushString(it.gyroscopeEvent_z);
            a.pushString(it.magnetometerEvent_x);
            a.pushString(it.magnetometerEvent_y);
            a.pushString(it.magnetometerEvent_z);
            a.pushString(it.latitude);
            a.pushString(it.longitude);
            a.pushString(it.time);
            a.pushString(it.accuracy);
            a.pushString(it.altitude);
            a.pushString(it.bearing);
            a.pushString(it.bearingAccuracyDegrees);
            a.pushString(it.speed);
            a.pushString(it.speedAccuracyMetersPerSecond);
            a.pushString(it.modelId);
            a.pushString(it.deltaBearing.toString());
            a.pushString(it.deltaBearing.toString());
            a.pushString(it.accelerationCurve.toString());
            fn.pushArray(a);
        })
        promise.resolve(fn);
    }

    @ReactMethod
    fun deleteReport(reportName:String,promise: Promise){
        db.locationPayloadDAO().deleteByReport(reportName)
        promise.resolve(true)
    }

    @ReactMethod
    fun setControlParams(min_speed:Double, min_acceleration:Double, min_bearing:Double,  promise: Promise){
        db.controlParamsDAO().setParams(
                ControlParams(
                        null,
                        min_speed,
                        min_acceleration,
                        min_bearing,
                )
        )
        promise.resolve(true)
    }


    init {
        Intrinsics.checkNotNullParameter(reactContext, "reactContext")

        db = Room.databaseBuilder(
                reactApplicationContext,
                AppDatabase::class.java, "yod"
        ).allowMainThreadQueries()
                .addMigrations(
                        Migrations.MIGRATION_1_2,
                        Migrations.MIGRATION_2_3,
                        Migrations.MIGRATION_3_4,
                )
                .fallbackToDestructiveMigration()
                .build()
    }

}
