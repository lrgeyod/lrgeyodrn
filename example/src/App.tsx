import YodController from 'lrge_yod_rn';
import React, {useState} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';


export default function App() {
  const [payload, setPayload] = useState({});
  const boardingCode = 'codeSample'+(Math.floor(Math.random() * (1000 - 0 + 1) + 0));
  const apiToken = ''
  const yodController = new YodController(apiToken, "94356549082", boardingCode);
  const onPress = () => {
    yodController.start();
  };

  const onPressStop = () => {
    yodController.stop();
  };

  const onPressLast = () => {
    yodController.getLastPayload().then((a)=>{
      console.log(a);
      setPayload(a);
    });;
  };
  const onPressScore = () => {
    yodController.getScore().then((a)=>{
      setPayload(a);
    });;
  };
  return (
    <View style={styles.container}>
      <Button
        title="Start!"
        color="#841584"
        onPress={onPress}
      />

      <Button
        title="Stop!"
        color="#841584"
        onPress={onPressStop}
      />

      <Button
        title="Get last!"
        color="#841584"
        onPress={onPressLast}
      />

      <Button
        title="Get Score!"
        color="#841584"
        onPress={onPressScore}
      />

      <Text style={{color:'black'}}>
        {JSON.stringify(payload)}
      </Text>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
