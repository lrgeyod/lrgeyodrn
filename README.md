# lrge_yod_rn

LRGE React Native component to communicate in LRGE YOD Services

## Installation

```sh
npm install lrge_yod_rn
```

## Setup 

### Android SDK Version

```
 minSdkVersion = 26
```

### Manifest 

Need to add permissions and register the services 

Add permissions:
```xml 
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
    <uses-permission android:name="android.permission.VIBRATE"/>

    <uses-permission android:name="android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />

    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>
    <uses-permission android:name="android.permission.POST_NOTIFICATIONS"/>
```

Register services: 

```xml 
<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version" />
<!-- Necessary for apps that target Android 9.0 or higher -->
<uses-library android:name="org.apache.http.legacy" android:required="false" />
<service
    android:name="com.lrgeyodrn.yod.EndlessService"
    android:enabled="true"
    android:stopWithTask="false"
    android:foregroundServiceType="location"
    android:exported="false">
</service>

<receiver android:enabled="true" android:name="com.lrgeyodrn.yod.StartReceiver"
    android:exported="true">
<intent-filter>
    <action android:name="android.intent.action.BOOT_COMPLETED"/>
</intent-filter>
</receiver>
```


## Usage

```js
import YodController from 'lrge_yod_rn';

const yodController = new YodController(apiToken, cpf, boardingCode);

yodController.start(); // start tracking

let lastPayload = await yodController.getLastPayload(); // last payload in tracking

yodController.stop(); // stop current tracking and send to server

let score = await yodController.getScore(); // score of the actual tracking (only after stopped)

```

# Check the sample in this repository

## YARN

YARN 1.22.15